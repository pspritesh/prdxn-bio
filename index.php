<?php
// error_reporting(E_ALL);
/*
* Punch time calculation logic.
* need to remove
*
*/
$isValidFile = 0;
$isFileSelected = 0;
$calculatedDataPresent = 0;
$isFormSubmitted = 0;
$FT1 = [2,4,10,14,16,23,37,38,45,53,54,65,95,97,99,105,107,108,116,121,137,140,146,149,151,152,154,155,156,158];
$FT2 = [1,3,6,8,9,11,12,13,19,20,22,24,29,30,31,35,41,44,47,49,50,51,52,55,57,67,77,78,79,85,86,88,90,91,98,102,109,113,114,115,117,119,120,123,124,125,126,127,128,129,130,132,133,134,135,136,138,139,142,143,144,147,148,150,153];
$holidays = ['01-Jan-2019','14-Jan-2019','26-Jan-2019','21-Mar-2019','06-Apr-2019','19-Apr-2019','01-May-2019','05-Jun-2019','12-Aug-2019','15-Aug-2019','24-Aug-2019','02-Sep-2019','02-Oct-2019','08-Oct-2019','27-Oct-2019','10-Nov-2019','25-Dec-2019'];
$crainQaD8 = [2,57,90,148];


// echo '<pre>';
// var_dump($holidays);
// echo '</pre>';
$summryArrayTotalWorkDuration = array();
$summryArrayTotalBreakDuration = array();
$summryArrayMissingEntries = array();
$summryAbuse = array();

if(isset($_POST['doCalculations'])) {
    $isFormSubmitted = 1;
    $fileArrayData = $_FILES['uploadedFile'];
    if(isset($_FILES['uploadedFile'])) {
        $isFileSelected = 1;
        $pathInfo = pathinfo($fileArrayData["name"]);
        $pathInfo = $pathInfo['extension'];
        if(($fileArrayData['type'] === 'text/csv') || ($fileArrayData['type'] === 'application/vnd.ms-excel')) {
            $isValidFile = 1;
            $file = fopen($fileArrayData["tmp_name"],"r");
            $outsideArrC = 0;
            $dataArray = array();
            $startOfArrPtr = array();
            while (!feof($file)) {
                $line = fgetcsv($file, 'Code:');
                if($line) {
                    $tmp = 0;
                    $dataArray[$tmp] = array();
                    foreach ($line as $cell) {
                        if ($cell) {
                            $tmpTxt = htmlspecialchars($cell);
                            if ($tmpTxt === 'Emp Code:') {
                                array_push($startOfArrPtr, $outsideArrC);
                            }
                            $dataArray[$outsideArrC][$tmp] = htmlspecialchars($cell);
                            $tmp++;
                        }
                    }
                    if ($tmp > 0) {
                        $outsideArrC++;
                    }
                }
            }

            fclose($file);
            $dataArrayCount = count($dataArray);
            $startOfArrPtrCount =  count($startOfArrPtr);
            $pointer4DataArray = 0;
            $nextPointer4DataArray = 0;
            
            $employeeCode = 0;


            for($tmp = $startOfArrPtr[0]; $tmp < $dataArrayCount; $tmp++) {
                $countOfSubArray = count($dataArray[$tmp]);
                $innerArrayDetails = $dataArray[$tmp];
                if($innerArrayDetails[0] == 'Emp Code:'){
                    $employeeCode = $innerArrayDetails[1];
                    $employeename = $innerArrayDetails[3];
                    $summryArrayTotalWorkDuration[$employeeCode] = array();
                    $summryArrayTotalBreakDuration[$employeeCode] = array();
                    $summryArrayTotalWorkDuration[$employeeCode]['Work Duration'] = array();
                    $summryArrayTotalBreakDuration[$employeeCode]['Break Duration'] = array();
                    $summryArrayMissingEntries[$employeeCode]['Missing Entries'] = array();
                    $summryAbuse[$employeeCode]['Abuse Entries'] = array();
                    continue;
                }
                if($countOfSubArray !== 13) {
                    continue;
                }
                
                if($innerArrayDetails[0] === 'Att. Date') {
                    array_push($dataArray[$tmp], 'break time', 'missing entries', 'abuse', 'name');
                    continue;
                }

                $attDate = $innerArrayDetails[0];
                $inTime = $innerArrayDetails[1];
                $outTime = $innerArrayDetails[2];
                $shift = $innerArrayDetails[3];
                $stdInTime = $innerArrayDetails[4];
                $stdOutTime = $innerArrayDetails[5];
                $workDuration = $innerArrayDetails[6];
                $ot = $innerArrayDetails[7];
                $totalDuration = $innerArrayDetails[8];
                $lateBy = $innerArrayDetails[9];
                $EarlyGoingBy = $innerArrayDetails[10];
                $Status = $innerArrayDetails[11];
                $punch_records = $innerArrayDetails[12];
                


                $punchedArray = explode(',',$punch_records);
                array_pop($punchedArray);
                $tmpCountPunchArr = count($punchedArray) - 1;
                $falseEntries = false;
                $falseEntriesData = '';
                $brkHours = 0;
                $brkMins = 0;
                
                if($totalDuration === '00:00') {
                    $tmpTotalTimeFrst = $punchedArray[0];
                    $tmpTotalTimeScnd = $punchedArray[$tmpCountPunchArr];
                    $tmpTotalTimeFrst = explode(':', $tmpTotalTimeFrst);array_pop($tmpTotalTimeFrst);
                    $tmpTotalTimeScnd = explode(':', $tmpTotalTimeScnd);array_pop($tmpTotalTimeScnd);
                    $tmpTotalTimeFrst = implode(":",$tmpTotalTimeFrst);
                    $tmpTotalTimeScnd = implode(":",$tmpTotalTimeScnd);
                    $tmpTotalTimeFrst1 = new DateTime($tmpTotalTimeFrst);
                    $tmpTotalTimeScnd1 = new DateTime($tmpTotalTimeScnd);
                    $intervalTFt = $tmpTotalTimeFrst1->diff( $tmpTotalTimeScnd1 );
                    $dataArray[$tmp][8] = $totalDuration = $intervalTFt->h . ':' . $intervalTFt->i;
                }

                for($i =0;$i<($tmpCountPunchArr+1);$i++) {
                    $susitT = $punchedArray[$i];
                    $susitT = explode(':', $susitT);array_pop($susitT);
                    $susitT = implode(":",$susitT);
                    $punchedArray[$i] = $susitT;
                }
                $punchedArray = array_unique($punchedArray);
                $filteredPunchRecords = array();
                foreach ($punchedArray as $avalue) {
                    array_push($filteredPunchRecords, $avalue);
                }
                $countOfFiltrArray = count($filteredPunchRecords) - 1;

                if($employeeCode == 62) {
                    $outTimeTmp = $filteredPunchRecords[$countOfFiltrArray];
                    $outTimeWithDate = new DateTime('2014-03-18 '.$outTimeTmp.':00');
                    $compareTimeWithDate = new DateTime('2014-03-18 22:30:00');
                    if($outTimeWithDate > $compareTimeWithDate) {
                        $interval = $outTimeWithDate->diff($compareTimeWithDate);
                        $hdiff = $interval->h;
                        $mdiff = $interval->i;
                        $ot = $hdiff . ':' . $mdiff;
                        $dataArray[$tmp][7] = '<strong style="color:red">'.$ot.'</strong>';
                    }
                }

                


                
                for($i=1; $i < $countOfFiltrArray; $i++) {
                    $susit = $filteredPunchRecords[$i];
                    $susout = $filteredPunchRecords[$i+1];
                    if($i === ($countOfFiltrArray - 1)) {
                        $falseEntries = true;
                        $falseEntriesData .= $susit . ',';
                        continue;
                    }
                    $val1 = '2014-03-18 '.$susit.':00';
                    $val2 = '2014-03-18 '.$susout.':00';
                    $datetime1 = new DateTime($val1);
                    $datetime2 = new DateTime($val2);
                    $interval = $datetime1->diff( $datetime2 );
                    $hdiff = $interval->h;
                    $mdiff = $interval->i;
                    if($hdiff === 1 AND $mdiff > 30 ) {
                        $falseEntries = true;
                        $falseEntriesData .= $susit . ',';
                    } else if($hdiff > 1) {
                        $falseEntries = true;
                        $falseEntriesData .= $susit . ',';
                    } else {
                        $brkHours += $hdiff;
                        $brkMins += $mdiff;
                        $i++;
                    }
                }
                $breakTime = 0;
                if($falseEntries) {
                    $brkMins = $brkMins+45;
                }
                if($brkHours > 0) {
                    $breakTime = ($brkHours + floor($brkMins / 60) ). ':' . ($brkMins % 60);
                }  else {
                    $breakTime = floor($brkMins / 60) . ':' . ($brkMins % 60);
                }
                
                $tmpDate1 = new DateTime($totalDuration);
                $tmpDate2 = new DateTime($breakTime);
                $tdCalclatedTime = $tmpDate1->diff( $tmpDate2 );
                $workDuration = $tdCalclatedTime->h . ':' . $tdCalclatedTime->i;

                $dataArray[$tmp][6] = $workDuration;

                array_push($dataArray[$tmp], $breakTime, $falseEntriesData);

                $inTimeTmp = $filteredPunchRecords[0];
                $inTimeWithDate = new DateTime('2014-03-18 '.$inTimeTmp.':00');
                if(in_array($employeeCode, $FT2)) {
                  if(in_array($employeeCode, $crainQaD8)) {
                    $compareInTimeWithDate = new DateTime('2014-03-18 12:30:00');
                  }else {
                    $compareInTimeWithDate = new DateTime('2014-03-18 12:15:00');
                  }
                }else {  
                  $compareInTimeWithDate = new DateTime('2014-03-18 10:15:00');  
                }
                $abusecell = '<strong>0:0</strong>';

                if(($inTimeWithDate > $compareInTimeWithDate) AND ($employeeCode != 62)) {
                  $interval = $inTimeWithDate ->diff($compareInTimeWithDate);
                  $hdiff = $interval->h;
                  $mdiff = $interval->i;
                  $abuse = $hdiff . ':' . $mdiff;
                  $abusecell = '<strong style="color:red">'.$abuse.'</strong>';
                  array_push($summryAbuse[$employeeCode]['Abuse Entries'], $abuse);
                }

                array_push($dataArray[$tmp], $abusecell, $employeename);
                array_push($summryArrayTotalWorkDuration[$employeeCode]['Work Duration'], $workDuration);
                array_push($summryArrayTotalBreakDuration[$employeeCode]['Break Duration'], $breakTime);
                if($falseEntriesData) {
                  array_push($summryArrayMissingEntries[$employeeCode]['Missing Entries'], $falseEntriesData);
                }                
                
            }

    $calculatedDataPresent = 1;
        }
    }
    
}
//$con = mysqli_connect("127.0.0.1","root","mysql","hr_calculations_employeesRecords");

//$file = fopen("DailyAttendanceReport.xls-17thAug.xls-Sheet1.csv","r");

function manupluteArray($givenArray, $givenKey) {
    foreach ($givenArray as $key => $values) {
    $totalWorkDuration = 0;

    foreach ($values as $key1 => $value1) {
        $givenArray[$key][$givenKey] = '';
        $sum = 0;
        $inTimeHoursSum = 0;
        $inTimeMinuteSum = 0;
        $outputTimeMinute = 0;
        $addHour = 0;
        foreach ($value1 as $key2 => $value2) {
           $workTime = $value2;
           $workTimeArray = explode(":", $workTime);
           $inTimeHoursSum = $inTimeHoursSum + $workTimeArray[0];
           $inTimeMinuteSum = $inTimeMinuteSum + $workTimeArray[1];
        }
        $outputTimeMinute = $inTimeMinuteSum % 60;
        $addHour = floor($inTimeMinuteSum / 60);
        $finalHoursSum = $inTimeHoursSum + $addHour;
        $finalMinuteSum = $outputTimeMinute; 
        $totalWorkDuration = $finalHoursSum.':'.$outputTimeMinute;
        $givenArray[$key][$givenKey] = $totalWorkDuration;
    }
  }

  return $givenArray;
}


function manupluteArrayCount($givenArray, $givenKey) {
  foreach ($givenArray as $key => $values) { 
     foreach ($values as $key1 => $value1) {
        $givenArray[$key][$givenKey] = '';
        $totalCount = 0;
        foreach ($value1 as $key2 => $value2) {
          $missEntriesArray = explode(',',$value2);
          array_pop($missEntriesArray);
          $totalCount = $totalCount + count($missEntriesArray);
        }
        $givenArray[$key][$givenKey] = $totalCount;
    }
  }
  return $givenArray;
}

function manupluteArrayAbuseCount($givenArray, $givenKey) {
  foreach ($givenArray as $key => $values) { 
     foreach ($values as $key1 => $value1) {
        $givenArray[$key][$givenKey] = '';
        $totalCount = 0;
        foreach ($value1 as $key2 => $value2) {
          $totalCount = $totalCount + count($value2);
        }
        $givenArray[$key][$givenKey] = $totalCount;
    }
  }
  return $givenArray;
}

$summaryWorkD =  manupluteArray($summryArrayTotalWorkDuration, 'Total Work Duration');
$summaryWorkB =  manupluteArray($summryArrayTotalBreakDuration, 'Total Break Duration');
$summaryMissE =  manupluteArrayAbuseCount($summryArrayMissingEntries,  'Total Missing Entries Count');
$summaryAbuseE =  manupluteArrayAbuseCount($summryAbuse,  'Total Abuse Entries Count');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
    <h1>Time Calculator!</h1>
        <form action="" name="calculateEmployeesTime"  method="post" enctype="multipart/form-data">
            <label for="fileToUpload">Please upload CSV timesheet: </label>
            <input type="file" accept=".csv" name="uploadedFile" id="fileToUpload" />
            <?php 
            if($isFormSubmitted AND !$isFileSelected) { 
                echo '<br/><span style="color:red">Please select file</span>';
            } else if($isFormSubmitted AND !$isValidFile) {
                echo '<br/><span style="color:red">Please use csv file</span>';
            } ?>
            <br /><input type="submit" name="doCalculations" value="Show Details"/>
        </form>
    <?php if($calculatedDataPresent AND $isFormSubmitted) : ?>
    <div class="container-fluid">
        <?php
        $outPutStr = '';
        $outPutStr .= '<h2>'. $dataArray[1][0] .'' .$dataArray[1][1]. '</h2>';
        foreach ($dataArray as $dataValue) {
            if($dataValue) {
                $availablerowDataCount = count($dataValue); 
                if($availablerowDataCount == 4) {
                    if($dataValue[0] === 'Emp Code:') {
                        $outPutStr .= '<table class="table"><caption>';
                        $outPutStr .= '</caption>';
                        $employeeCode = $dataValue[1];

                    }
 
                }else if($availablerowDataCount == 10) {
                  if(in_array($dataValue[0], $holidays)) {
                    if(in_array($employeeCode, $FT2)) {  
                      $workTimeArray = explode(":", $summaryWorkD[$employeeCode]['Total Work Duration']);
                      $inTimeHoursSum = 7 + $workTimeArray[0];
                      $inTimeMinuteSum = 30 + $workTimeArray[1];
                      $outputTimeMinute = $inTimeMinuteSum % 60;
                      $addHour = floor($inTimeMinuteSum / 60);
                      $finalHoursSum = $inTimeHoursSum + $addHour;
                      $finalMinuteSum = $outputTimeMinute;
                      $newTotalWorkD = $finalHoursSum.':'.$finalMinuteSum;
                      $summaryWorkD[$employeeCode]['Total Work Duration'] = $newTotalWorkD; 
                    }
                    else if(in_array($employeeCode, $FT1)) {
                     $workTimeArray = explode(":", $summaryWorkD[$employeeCode]['Total Work Duration']);
                      $inTimeHoursSum = 8 + $workTimeArray[0];
                      $inTimeMinuteSum = 30 + $workTimeArray[1];
                      $outputTimeMinute = $inTimeMinuteSum % 60;
                      $addHour = floor($inTimeMinuteSum / 60);
                      $finalHoursSum = $inTimeHoursSum + $addHour;
                      $finalMinuteSum = $outputTimeMinute;
                      $newTotalWorkD = $finalHoursSum.':'.$finalMinuteSum;
                      $summaryWorkD[$employeeCode]['Total Work Duration'] = $newTotalWorkD; 
                    }
                  }
                }else if($availablerowDataCount == 17) {
                    if($dataValue[0] === 'Att. Date') {
                        $outPutStr .= '<thead><tr>';
                        $outPutStr .= '<th>' .$dataValue[16]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[0]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[1]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[2]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[3]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[6]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[13]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[14]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[8]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[11]. '</th>';
                        $outPutStr .= '<th>' .$dataValue[12]. '</th>';
                        if($employeeCode == 62) {
                            $outPutStr .= '<th>' .$dataValue[7]. '</th>';
                        }
                        $outPutStr .= '<th>' .$dataValue[15]. '</th>';
                        $outPutStr .= '</tr></thead><tbody>';
                    } else {
                        $outPutStr .= '<tr>';
                        $outPutStr .= '<td>' .$dataValue[16]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[0]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[1]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[2]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[3]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[6]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[13]. '</td>';
                        $outPutStr .= '<td style="color:red">' .$dataValue[14]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[8]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[11]. '</td>';
                        $outPutStr .= '<td>' .$dataValue[12]. '</td>'; 
                        if($employeeCode == 62) {
                            $outPutStr .= '<td>' .$dataValue[7]. '</td>';
                        }  
                        $outPutStr .= '<td>' .$dataValue[15]. '</td>';
                        $outPutStr .= '</tr>';
                    }
                } else if($availablerowDataCount == 1) {
              $outPutStr .= '<tr>';
              $outPutStr .= '<td>Total Work Duration:- '.$summaryWorkD[$employeeCode]['Total Work Duration']  .'</td>';
              $outPutStr .= '<td>Total Break Duration:- '.$summaryWorkB[$employeeCode]['Total Break Duration']  .'</td>';
              // $outPutStr .= '<td>Type A Abuse Entries Count:- '.$summaryAbuseE[$employeeCode]['Total Abuse Entries Count']  .'</td>';
                // if(in_array($employeeCode, $FT2)) {
                //   if($summaryWorkD[$employeeCode]['Total Work Duration'] < '37.30') {
                //     $outPutStr .= '<td>Type B Abuse :- 1</td>';
                //     $outPutStr .= '<td>Total Abuse Count :-' . ($summaryAbuseE[$employeeCode]['Total Abuse Entries Count'] + intval(1) ). '</td>';
                //   }else {
                //    $outPutStr .= '<td>Type B Abuse :- 0</td>';
                //    $outPutStr .= '<td>Total Abuse Count :- ' . ($summaryAbuseE[$employeeCode]['Total Abuse Entries Count']). '</td>';                   
                //   }
                // }else if(in_array($employeeCode, $FT1)) {
                //   if($summaryWorkD[$employeeCode]['Total Work Duration'] < '42.30') {
                //     $outPutStr .= '<td>Type B Abuse :- 1</td>';
                //     $outPutStr .= '<td>Total Abuse Count :-' . ($summaryAbuseE[$employeeCode]['Total Abuse Entries Count'] + intval(1) ). '</td>';
                //   }else {
                //    $outPutStr .= '<td>Type B Abuse :- 0</td>';
                //    $outPutStr .= '<td>Total Abuse Count :-' . ($summaryAbuseE[$employeeCode]['Total Abuse Entries Count']). '</td>';                   
                //   }
                // }
              $final_abuse_abuse = 0;
              if(in_array($employeeCode, $FT2)) {
                $outPutStr .= '<td>Type A Abuse Points:- '.$summaryAbuseE[$employeeCode]['Total Abuse Entries Count']  .'</td>';
                $outPutStr .= '<td>Type B Abuse Points:-  NA </td>';
                $final_abuse_abuse = $final_abuse_abuse + $summaryAbuseE[$employeeCode]['Total Abuse Entries Count'];
              }else {
                $outPutStr .= '<td>Type A Abuse Points:- NA </td>';
                $outPutStr .= '<td>Type B Abuse Points:- '.$summaryAbuseE[$employeeCode]['Total Abuse Entries Count']  .' </td>';
                $final_abuse_abuse = $final_abuse_abuse + $summaryAbuseE[$employeeCode]['Total Abuse Entries Count'];
              }
              $outPutStr .= '<td>Type C Abuse Points:- '.$summaryMissE[$employeeCode]['Total Missing Entries Count']  .'</td>';
              $final_abuse_abuse = $final_abuse_abuse + $summaryMissE[$employeeCode]['Total Missing Entries Count'];
              if(in_array($employeeCode, $FT2)) {
                  if($summaryWorkD[$employeeCode]['Total Work Duration'] < '37.30') {
                    $outPutStr .= '<td>Type D Abuse Points:- 2</td>';
                     $final_abuse_abuse = $final_abuse_abuse + 2;
                  }
                  else {
                   $outPutStr .= '<td>Type D Abuse Points:- 0</td>'; 
                  }
              }else {
                if($summaryWorkD[$employeeCode]['Total Work Duration'] < '42.30') {
                    $outPutStr .= '<td>Type D Abuse Points:- 2</td>';
                    $final_abuse_abuse = $final_abuse_abuse + 2;
                  }
                  else {
                   $outPutStr .= '<td>Type D Abuse Points:- 0</td>'; 
                  }
              } 
              $outPutStr .= '<td>Total Penalty Points :-  '.$final_abuse_abuse.'</td>';   
              $outPutStr .= '</tr></tbody></table>'; 
                }
            }
            
        }
        echo $outPutStr;
        
        ?>
        
<!--        <table>
            <caption>
                    This is an example table, and this is its caption to describe the contents.
                </caption>
                <thead>
                    <tr>
                        <th>Table heading</th>
                        <th>Table heading</th>
                        <th>Table heading</th>
                        <th>Table heading</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                    </tr>
                    <tr>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                        <td>Table cell</td>
                    </tr>
                </tbody>
            </table>-->
    </div>
    
    <?php endif; ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>

